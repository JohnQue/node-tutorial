module.exports = {
    HTML : function(title, description, list, body){
        return `
        <!doctype html>
        <html>
            <head>
                <title>WEB</title>
                <meta charset="utf-8">
            </head>
            <body>
                <h1><a href="/">WEB</a></h1>
                <ol>
                    ${list}
                </ol>
                ${body}
                <h1>${title}</h1>
                ${description}
            </body>
        </html>
        `;
    },
    List : function(list){
        var string = '';
        for(var key in list){
            string += `<li><a href="/?id=${list[key]}">${list[key]}</a></li>`;
        }
        return string;
    }
}
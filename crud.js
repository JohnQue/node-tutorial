var http = require('http');
var fs = require('fs');
var url = require('url');
var qs = require('querystring');
var template = require('./lib/template.js');

var app = http.createServer(function(request, response){
    var _url = request.url;
    var pathname = url.parse(_url, true).pathname;
    var queryData = url.parse(_url, true).query;

    if(pathname === '/'){
        if(queryData.id === undefined){
            fs.readdir('./data', function(err, filelist){
                var list = template.List(filelist);
                var HTML = template.HTML('Welcome', 'Hello, Node.js!', list, '<a href="/create">create</a>');
                response.writeHead(200);
                response.end(HTML);
            });
        }else{
            fs.readdir('./data', function(err, filelist){
                fs.readFile(`./data/${queryData.id}`, 'utf8', function(err, data){
                    var list = template.List(filelist);
                    var title = queryData.id;
                    var description = data;
                    var HTML = template.HTML(
                            title, 
                            description, 
                            list, 
                            `<a href="/create">create</a>
                            <a href="/update?id=${title}">update</a>
                            <form action="/delete" method="POST">
                                <input type="hidden" name="id" value="${queryData.id}">
                                <input type="submit" value="delete" onclick="if(!confirm('삭제하시겠습니까?')){return false;}">
                            </form>
                            `);
                    response.end(HTML);
                });
            });
        }
    }else if(pathname === '/create'){
        fs.readdir('./data', function(err, filelist){
            var list = template.List(filelist);
            var HTML = template.HTML(          
                'Create', 
                `
                <form action="/create_process" method="POST">
                    <p><input type="text" name="title" placeholder="title"></p>
                    <p><textarea name="description" placeholder="description"></textarea></p>
                    <p><input type="submit" value="create"></p>
                </form>
                `,
                list, 
                '');
            response.writeHead(200);
            response.end(HTML);
        });
    }else if(pathname === '/update'){
        fs.readdir('./data', function(err, filelist){
            fs.readFile(`./data/${queryData.id}`, function(err, description){
                var list = template.List(filelist);
                var title = queryData.id;
                var description = description;  
                var HTML = template.HTML(          
                    'Update', 
                    `
                    <form action="/update_process" method="POST">
                        <input type="hidden" name="id" value="${queryData.id}">
                        <p><input type="text" name="title" placeholder="title" value=${title}></p>
                        <p><textarea name="description" placeholder="description">${description}</textarea></p>
                        <p><input type="submit" value="update"></p>
                    </form>
                    `,
                    list, 
                    '');
                response.writeHead(200);
                response.end(HTML);
            });
        });
    }else if(pathname === '/delete'){
        var body = '';
        request.on('data', function(data){
            body += data;
        });
        request.on('end', function(){
            var post  = qs.parse(body);
            var id = post.id;
            fs.unlink(`./data/${id}`, function(err){
                if(err) throw err;
                response.writeHead(302, {Location: `/`});
                response.end();
            });
        });
    }else if(pathname === '/create_process'){
        var body = '';
        request.on('data', function(data){
            body += data;
        });
        request.on('end', function(){
            var post = qs.parse(body);
            var title = post.title;
            var description = post.description;
            fs.writeFile(`data/${title}`, description, 'utf8', function(err){
                if(err) throw err;
                response.writeHead(302, {Location: `/?id=${title}`});
                response.end();
            });
        });
    }else if(pathname === '/update_process'){
        var body = '';
        request.on('data', function(data){
            body += data;
        });
        request.on('end', function(){
            var post = qs.parse(body);
            var id = post.id;
            var title = post.title;
            var description = post.description;
            fs.rename(`data/${id}`, `data/${title}`, function(err){
                fs.writeFile(`data/${title}`, description, 'utf8', function(err){
                    if(err) throw err;
                    response.writeHead(302, {Location: `/?id=${title}`});
                    response.end();
                });
            });
        });
    }
});
app.listen(3000);